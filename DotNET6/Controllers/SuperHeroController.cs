﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DotNET6.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperHeroController : ControllerBase
    {
        private readonly AppDbContext _db;

        public SuperHeroController(AppDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<ActionResult<List<SuperHero>>> Get()
        {
            return Ok(await _db.SuperHeroes.ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SuperHero>> Get(int id)
        {
            var hero = await _db.SuperHeroes.FindAsync(id);
            if (hero == null)
                return BadRequest("Tidak ditemukan");
            return Ok(hero);
        }

        [HttpPost]
        public async Task<ActionResult<List<SuperHero>>> AddHero(SuperHero hero)
        {
            try
            {
                _db.SuperHeroes.Add(hero);
                await _db.SaveChangesAsync();
                return Ok(await _db.SuperHeroes.ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
           
        }

        [HttpPut]
        public async Task<ActionResult<List<SuperHero>>> UpdateHero(SuperHero req)
        {
            var dbHero = await _db.SuperHeroes.FindAsync(req.Id);
            if (dbHero == null)
                return BadRequest("Tidak ditemukan");

            dbHero.Name = req.Name;
            dbHero.FirstName = req.FirstName;
            dbHero.LastName = req.LastName;
            dbHero.Place = req.Place;

            await _db.SaveChangesAsync();
            return Ok(await _db.SuperHeroes.ToListAsync());
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<List<SuperHero>>> Delete(int id)
        {
            var dbHero = await _db.SuperHeroes.FindAsync(id);
            if (dbHero == null)
                return BadRequest("Tidak ditemukan");

            _db.SuperHeroes.Remove(dbHero);
            await _db.SaveChangesAsync();
            return Ok(await _db.SuperHeroes.ToListAsync());
        }
    }
}
