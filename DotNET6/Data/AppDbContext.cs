﻿using Microsoft.EntityFrameworkCore;

namespace DotNET6.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<SuperHero> SuperHeroes { get; set; }

    }
}
